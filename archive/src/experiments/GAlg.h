struct sGenome
{
	vector <double> v_Genes;
	double d_Fitness;
	SGenome():d_Fitness(0){}
	SGenome(vector <double> w, double f):v_Weights(w),d_Fitness(f){}
	friend bool operator < (const SGenome & lhs, const SGenome & rhs)
	{ return (lhs.d_Fitness<rhs.d_Fitness);}
};

class cGAlg
{
	private:
		vector <SGenome> mv_Pop; //the chromosomes of the population
		int  mi_Genes;//number of genes in each genome
		double md_PopFitness;
		double md_BestFitness;
		double md_AvgFitness;
		double md_WorstFitness;
		int mi_Fittest;
		double md_Mutation;//mutation rate
		double md_Crossover;//cross-over rate
		int mc_Generation;//generation counter

		//virtual functions that must be over-ridden
		void virtual crossover( const sGenome &mom,
					const sGenome &dad,
					sGenome &elder,
					sGenome &younger)=0;
		//handles the simulation of the "mating" of two genomes
		void virtual mutate(sGenome &gnome)=0;
		void virtual mateing_dance()=0;//determines which members
						//get chosen as parents
		void virtual calculate_fitnesses()=0;
	public:
		cGAlg(	int pop_size,
			double m,
			double c,
			int g):	md_Mutation(mutation),
				md_Crossover(c),
				mi_Genes(g);
		void virtual Epoch()=0;
		double BestFit() const {return md_BestFitness;}
		sGenome getBest() const {return mv_Pop[0];}//assumes sorted
							   //decending fitness
};
