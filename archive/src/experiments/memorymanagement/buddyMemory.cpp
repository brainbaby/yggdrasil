/**************************************************************************
* Yggdrasil project 2015						  *
*									  *
* A memory manager, based on the buddy memory model. Adapted by Jeremy    *
* Tinker from files created by Cassie Chin for a school project. The      *
* major change is translating a list data structure into a tree.	  *
*									  *
* Usage:								  *
* memmgr *mgr=new memmgr(100);//allocates 128 kB			  *
* int bytes=64;								  *
* int *pointer_to_array=(int *) mgr->malloc(bytes);			  *
* if (pointer_to_array) mgr->free(pointer_to_array);			  *
**************************************************************************/
#include "buddyMemory.h"

block::block(char * begin, int bytes, block* p)
{
	this->parent=p;
	this->start=begin;
	this->sz=bytes;
	this->end=this->start+this->sz;
	this->left=this->right=NULL;
	this->used=0;
	this->allocated=false;
}
block::~block()
{
	if(this->children())
	{
		delete this->left;
		delete this->right;
	}
	delete this->start;
	delete this->end;
	delete this->parent;
}
bool block::childAlloc()
{
	if (this->left->free() || this->right->free())
		return false;
	this->allocated=true;
	return true;
}
//info about this block
bool block::children()
{
	if (this->left)
		return true;
	return false;
}
bool block::owned(void * address)
{
	return (address > this->start && address < this->end);
}
//change the status of this block
char *block::alloc(int request)
{
	if(this->sz<request)
		throw memException e("Attempt to allocate more memory than available in block");
	if(this->allocated)
		throw memException e("Attempt to allocate memory block twice");
	if(this->children())
	{
		if(this->left->free() && this->right->free())
		{
			try
			{
				this->left->alloc(request/2);
				this->right->alloc(request/2);
			}catch(memException e){throw e;}
		}
		else
			throw memException e("Can't allocate all of memory block");
	}
	this->used=request;
	this->allocated=true;
	this->parent->childAlloc();
}
void block::split()//divides and creates children
{
	if(this->allocated)
		throw memException e("Attempt to divide already allocated memory block");
	if(!this->left && this->sz>1)//has no children and can be divided
	{
		this->left=new block(this->start, this->sz/2, this);
		this->right=new block(this->start+this->sz/2+1, this->sz/2, this);
	}
}
void dealloc()
{
	if(this->allocated==false)
		throw memException e("Attempted double free");
	this->allocated=false;
	this->parent->allocated=false;
}
//tree navigation
block * block::down()//returns first free child, left preference
{
	if(this->left->free())
		return this->left;
	return this->right;
}
block * block::find(char * address)
{
	if(this->children())
	{
		if(address>this->start+this->sz/2)
			return this->right;
		else return this->left;
	}
	return NULL;
}
memmgr()//default, grabs 1024 kB
{//TODO: figure out how to do this without a system call, on startup for 
	//the OS
	memmgr(1024);
}
memmgr(int sz)	//custom, grabs nearest power of 2 in kB
{
	char *mem=(char *)malloc(sz*1024);
	root=new block(mem,sz*1024);
}
void * malloc(int request)
{
	block *curr=root;
	while(curr->size()>request && curr->free())
	{
		curr=curr->down();
	}
	curr=curr->up();
	try
	{
		return (void *)(curr->alloc(request));
	}catch(memException e)
	{
		throw (e);
	}
}
bool free(void * address)
{
	block *curr=root;
	while(curr->find((char*)address)!=NULL)
		curr=curr->find((char*)address);
	try
	{
		return curr->dealloc();
	}catch(memException e){throw(e);}
}
void print();	//traverses the tree, and prints its status
~memmgr();
