/**************************************************************************
* Yggdrasil project 2015						  *
*									  *
* A memory manager, based on the buddy memory model. Adapted by Jeremy    *
* Tinker from files created by Cassie Chin for a school project. The      *
* major change is translating a list data structure into a tree.	  *
*									  *
* Usage:								  *
* memmgr *mgr=new memmgr(100);//allocates 128 kB			  *
* int bytes=64;								  *
* try{									  *
* 	int *pointer_to_array=(int *) mgr->malloc(bytes);		  *
* 	if (pointer_to_array) mgr->free(pointer_to_array);		  *
* }									  *
* catch(memException e){cout << e.what();}				  *
**************************************************************************/

#include <stdexcept>
#ifndef BUDDY_MEMORY_H
#define BUDDY_MEMORY_H

using namespace std;

class memException : public exception//for notifying users that memory is n$
{
        public:
            explicit memException(const char* message):msg_(message){}
            explicit memException(const std::string& message):msg_(message)$
            virtual ~memException() throw () {}
            virtual const char* what() const throw(){return msg_.c_str();}

        protected:
            std::string msg_;
};

class block {
	private:
		char *start;
		char *end;
		block *parent;
		block *left, *right; //children
		int sz; //size in bytes
		bool allocated //denotes whether the current block is alloc'd
		int used; //size of used bytes, for tracking internal frag
		bool childAlloc();
	public:
		block(char * start, int bytes, block p);
		~block();

		//info about this block
		int size(){return this->sz;}
		bool children();
		bool free(){return !this->allocated;}
		int frag(){return this->sz-this->used;}
		bool owned(void *);//finding for free

		//change the status of this block
		char *alloc(int request);
		void split();//divides and creates children
		void dealloc();

		//tree navigation
		block *up(){return this->parent;}
		block *down();//returns first free child, left preference
		block *find(char *);
}

class memmgr{
	private:
		block * root	//root block, size of "all" memory
	public:
		memmgr();	//default, grabs 1024 kB
		memmgr(int);	//custom, grabs nearest power of 2 in kB
		void * malloc(int);
		bool free(void *);
		void print();	//traverses the tree, and prints its status
		~memmgr();
};
#endif
