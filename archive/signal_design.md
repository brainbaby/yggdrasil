Signal Design Considerations
============================
There are several ways that signalling happens between neurons: electrically, 
chemically using neurotransmitters, or chemically using hormones. Electrical
signalling involves direct transmission of membrane potential from one neuron
to another. 

Neurotransmitter signalling is much more varied, involving the release of 
excitory(+) and inhibitory(-) neurotransmitters. In order for these chemical
messages to be effectively transfered, the post-synaptic neuron(PSN) must have
the correct transport proteins present in its membrane. It must be able to 
"hear" the message being sent. In additon to this the message can only reach 
the PSN by diffusion, the rate of which is different for at least each class 
of neurotransmitter. In general, there are three size-based classes of 
transmitters: small molecule (eg. CO2, NO, acetlycholine), amino acids 
(glutamate, GABA, glycine), and peptides (somatostatin, substance P, opioids).
There may be as few as 30 transmitters, or more than 100, though six are 
assigned as the most important and best well known: serotonin, acetylcholine, 
GABA, dopamine, epinephrine, and norepinephrine. Many of the rest are believed
to fufill moderator, controller, or modifier rols. All of these, except GABA, 
fall into the small molecule category. GABA, serotonin are purely inhibitory; 
epinephrine and norepinephrine are purely excitory. Dopamine and acetylcholine 
may be excitory or inhibitory, possibly depending on the type of receptor that 
it activates.

These can additionally be divided by the receptors they use, either ligand
gated receptors or G-protein coupled receptors(GPCR). Ligand gated receptors are
the more easily understood in that they directly manage the PSN membrane 
potential through opening and closing a channel for necessary ions to cross.

A GPCR, on the other hand, interacts much more complexly. Each receptor gathers
and builds a G-protein inside the neuron. When its ligand activates the receptor
it releases its protein to start an intraneuron signal cascade that ultimately 
activates an ion channel. 

Hormone signalling is similar to neurotransmitter signalling, and can use
related, or at least similar, receptors. However, hormonal interaction effects
more than just the short term behavior of a neuron. It is believed that hormones
affect general behavior and gene expression within a neuron, but has no major
effect on membrane charge. There are believed to be only about 50 human 
hormones, many of which do not directly effect the brain. 

Limitations & Design
--------------------
The maximum length of a message is constrained by the size of the CPU registers.
This may be 32 or 64 bits in contemporary personal computers. It is likely that
32-bits is the maximum that can be expected. This limits the number of signals
that can be sent to 2^32(4,294,967,296). This register should only be loaded by
synapse terminals and ignored by all other portions of a neuron.

Because the electrical signalling is either excitory or non-affective, it can 
be expressed in a single bit, leaving 2^31 (2,147,483,648) signals that can be
sent. Using the same encoding method is impossible for neurotransmitters 
because there are too many transmitters that could possibly exist, in too many
combinations, with no guarantee that any particular combination cannot or should
not exist. 

Because of this, some type of  encoding must be done. Prime encoding, which 
assigns a prime number to each known transmitter, and multiplies them to encode
a specific state for the simulated synapse. Using this method, 173 different
transmitters can be encoded in groups of three or less, and in 31 bits or less

Hormone signalling will have to be done in a specific "hormone" register, or 
registers, using similar encoding, but using the first 14 primes instead of the
first 174. This makes it possible to group six hormones at a time. As this may 
not be enough to accurately simulate complex growth periods, a second hormone
register may be needed. If a second register is used, additional positional
encoding can be used and expanding the available types of hormones to 28, over
half those believed to be present in the human body.
