Design Considerations
=====================

There are three contexts in which processes/neurons/compartments must operate: growth, stabilization, operation, and death. The critical behavior in each context is significantly different from the others. In the growth context, the primary behavior is the extention and direction of connecting axional and dendritic fibers to their targets. Stabilization involves the selective reduction of poorly functioning synapses in order to ensure appropriate function of the neural circuit, similar to the training/learning phase of a standard artificial neural network. The operational context is the majority of what is considered neural behavior: passing, integrating/resonating, and generating signals. Orderly death (apoptosis) is the process of removing unused neurons of specific types. 

There are many different types of neurons, at least several hundred. Each category potentially serves a specific purpose in the physical and circuit structure of the brain. 
=======


Required Information for Each Neuron/Compartment
------------------------------------------------
*memory
	+inputs
	+behavior
	+outputs
*id
*next neuron/comparment to load
*cycles remaining
*priority??
*deadline??

Operations for Each Neuron/Compartment
--------------------------------------
read inputs
"transport" inputs
process inputs
"transport" outputs
release signals
