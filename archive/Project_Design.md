Overall Goal: Understand Conciousness
Prime question: is conciousness an inherent quality of the brain, or is it something external
Hypothesis: Conciousness is inherent to the patterns and fluctuations in those patterns of activation in the human brain.
Test: Create a model and simulation as close as possible to the human brain, that functionally operates identically.
	If the model is able to interact with human beings and its environment in a manner that convinces a significant number of people that it is  			concious, then conciousness is likely a deterministic process resulting in the pattern and fluctuations of that pattern of activations 			in the human brain. 
	Otherwise, human conciousness cannot necessarily be classified as a deterministic product of the electricl/chemical/physical interactions in 			the human body
Potential behavioral determinates of Conciousness:
"Physiological" need satisfaction
Pattern seeking/Reasoning
Opinion forming

Method Stage 1:
	Survey of philisophical thought on conciousness, will, self, and the soul with an eye towards specific "universal" criteria of a "self"
	Survey of genetic algorithm and  artifical neural network methods, procedures, and best practices to increase familiarity and proficiency with 			the topic.
Method Stage 2.1:
	Design, implement, and monitor the operation of a simple web-scraper and text analyzer to determine/identify additional criteria or symbolic 			manipulations present in the english-speaking human population of the web
	Analyze data inputs, outputs, and relationships to mathematically describe and establish the validity of any discovered critera and/or 			symbolic manipulations, and any other significant features of the data.
Method Stage 2.2:
	Modify the scraper-analyzer to generate appropriate responses to typed imput
	Analyze input, output, and relationship data to identify any significant changes from the Stage 2 results
Method Stage 3:
	Brief, laymen's survey of neurology, neuroscience, psychology, and brain medicine to identify additional critical behaviors the system must 			exhibit and critical structures that are potentially necessary to the final system.
	Design, implement, train, test, and monitor the operation of various perceptrons that will be required to interface with the final system: 
		vision: ability to classify and identify objects in view
		hearing: ability to classify and identify sounds within hearing
		memory: abstraction and storage of critical features
		speech: ability to reproduce memorized sounds, produce unique sounds
		web-interfaces: ability to communicate with external computers via various telecommunication protocols
	Abstract network patterns from perceptrons for final system
Method Stage 4:
	Design, implement, train, and test perceptrons to build similar association matrices as those determined in Stage 2.1
	Link and test interaction of vision, hearing memory, speech, and web-interface and associational perceptrons
	Monitor the operation of combined model
	Analyze resulting network for significant differences from Stage 2 models, and mathematically describe and possibly explain any new data 			features
	Evaluate the feasibility of porting model closer to hardware level
	Evaluate the feasibility of porting model to a more accurate depiction of neuronal interactions
