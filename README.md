# Yggdrasil README #
###CURRENT STATUS###
Research and experimentation

### Setup ###
Pending design and implementation

### Repository Structure###
+ research/
	+ Contains Reference material and text files that contain:
 		* Citations for material for future inspection and assessment
		* A list of reference material, status, and evaluation of the material
		* A list of partially prioritized research questions and their status (potentially with links to answers)
+ research/notes/
	+ Contains text files that:
		* Describe and/or translate information into algorithm/program behavior or data structure requirements
		* Describe potential experimental or implementation approaches for specific problems
+ src/
	* Contains source code for implementation and experimentation
+ src/experiments
	* Contains directories of completed experiments and their results
+ src/implementation
	* Contains directories with current implementations of operating system elements
+ design/
	* Contains design notes for specific operating system elements
+ docs/
	* Documentation on elements of the OS and their interactions

### Project Description ###
Yggdrasil is a project to design, develop, implement, and grow an operating system modeled on the human brain. 

Further details pending initial research