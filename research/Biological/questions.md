### Unprioritized Research Questions ###
Brain Related
=============
	How long is a neuron's life cycle, and how does it work?
	How do neurons deal with the Hayflick limit if they do not regenerate?
	How do the specialized portions of the human brain develop?
	What affect do other human systems have on neuron development?
	What affect do other human systems have on neuron operation?

Operating System Related
========================
	How are data presented from devices?
	What are the time constraints for each neuron to have an accurate simulation?
	Do the time constraints ever change?
	Is paging/segmenting memory feasible?
	Is simulation of a neuron life cycle necessary?
	Is simulation of glial cells necessary? If so, which ones and to what purpose?
	What other cells must be simulated?

Hardware Related
================
	How many processing cores are necessary?
	Is distributed computing feasible?
	How much memory is needed to simulate trillions of neurons?
	Is context switching feasible?
	Is a hard-drive needed? If so, what role does it play, and what are the requirements for it?
	What cpu/gpu/bridge speed is necessary? And is it available?

### Partially Answered Questions ###
	How do neurons work? (in neural-coding and neural-signalling)
	What causes the so-called "brain waves", and do these need to be coded or are they naturally occuring?(in neural coding)
	What are the various methods of inter- and intra-neuron signalling and how do they work? (in neural signalling)
	What determines the timing, degree, and type of transmitter production? (in neural signalling and neural coding)
	What are critical elements of an operating system? (design.md)
